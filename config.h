/* See LICENSE file for copyright and license details. */

/* Constants */
#define TERMINAL "alacritty"
#define TERMCLASS "Alacritty"

/* appearance */
static const unsigned int borderpx  = 2;		/* border pixel of windows */
static const unsigned int snap      = 10;		/* snap pixel */
static const int swallowfloating    = 0;		/* 1 means swallow floating windows by default */
static const unsigned int gappih    = 20;		/* horiz inner gap between windows */
static const unsigned int gappiv    = 20;		/* vert inner gap between windows */
static const unsigned int gappoh    = 20;		/* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 20;		/* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;		/* 1 means no outer gap when there is only one window */
static const int showbar            = 1;		/* 0 means no bar */
static const int topbar             = 1;		/* 0 means bottom bar */
static const int user_bh            = 0;		/* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Fira Code:size=10", "JoyPixels:pixelsize=10:antialias=true:autohint=true" };
static const char dmenufont[]       = "Fira Code:size=10";
static const char col_gray1[]       = "#2b2c26"; // Default #222222
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#214244";
static const unsigned int baralpha = OPAQUE; // Default 0xd0
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
/*  class 			instance    title       tags mask     isfloating   isterminal 	noswallow			monitor    float x,y,w,h 		floatborderpx 	 scratch key*/
/*  { "Gimp",			NULL,       NULL,       	0,            	1,        	  0, 					0, 					-1,			50,50,500,500, 					5,						 0  }, */
/*  { "Firefox", 	NULL,       NULL,       	1 << 8,       	0,        	  0,					0, 					-1,			50,50,500,500, 					5,						 0	}, */
	{ TERMCLASS,	NULL,       NULL,       	0,			 				0,						1, 					0,					-1,     100,100,500,500, 				3,						 0	},
	{ NULL, 			NULL,   "scratchpad",   	0,							1,          	0, 					0, 					-1,			480,270,960,540, 				3, 						's' },
	{ NULL,      	NULL,  "Event Tester", 		0, 			  			0,          	0,      		1,					-1, 		0,	0,	0,	0, 					0, 						 0 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile }, 				/* Default: Master on left, slaves on right */
	{ "TTT",      bstack }, 			/* Master on top, slaves on bottom */

	{ "[@]",      spiral }, 			/* Fibonacci spiral */
	{ "[\\]",     dwindle },			/* Decreasing in size right and leftward */

	{ "H[]",      deck },				/* Master on left, slaves in monocle-like mode on right */
	{ "[M]",      monocle },			/* All windows on top of eachother */

	{ "|M|",      centeredmaster },			/* Master in middle, slaves on sides */
	{ ">M>",      centeredfloatingmaster },	/* Same but master floats */

	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },

	/* ==== Other Unused Layouts ==== */
	/* { "===",      bstackhoriz }, */
	/* { "HHH",      grid }, */
	/* { "###",      nrowgrid }, */
	/* { "---",      horizgrid }, */
	/* { ":::",      gaplessgrid }, */

};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

/* First arg only serves to match against key in rules */
static const char *scratchpadcmd[] = {"s", TERMINAL, "-t", "scratchpad", NULL}; 

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier								key								function							argument */
	
	// Window Manager Binds
	{ MODKEY,             		XK_Return,				spawn,    						{.v = termcmd } },
	{ MODKEY,                 XK_a,							togglescratch,				{.v = scratchpadcmd } },
	{ MODKEY,                 XK_b,							togglebar,						{0} },
	{ MODKEY,           			XK_v,  						focusmaster,					{0} },
	{ MODKEY,                 XK_j,							focusstack,						{.i = +1 } },
	{ MODKEY,                 XK_k,							focusstack,						{.i = -1 } },
	{ MODKEY,                 XK_h,							setmfact,							{.f = -0.05} },
	{ MODKEY,                 XK_l,							setmfact,							{.f = +0.05} },
	{ MODKEY,                 XK_o,							incnmaster,						{.i = +1 } },
	{ MODKEY|ShiftMask,       XK_o,							incnmaster,						{.i = -1 } },
	{ MODKEY,                 XK_space,					zoom,									{0} },
	{ MODKEY,                 XK_Tab, 					view,     						{0} },
	{ MODKEY,             		XK_q,   					killclient,   				{0} },
	{ MODKEY|ShiftMask,				XK_q,   					quit,         				{0} },
	{ MODKEY,									XK_g,   					togglegaps,   				{0} },
	{ MODKEY|ShiftMask,				XK_g,   					defaultgaps,  				{0} },
	{ MODKEY,									XK_z,							incrgaps,							{.i = +3 } },
	{ MODKEY,									XK_x,							incrgaps,							{.i = -3 } },
	{ MODKEY,									XK_s,							togglesticky,					{0} },
	{ MODKEY|ShiftMask,				XK_s,							togglealwaysontop,		{0} },
	{ MODKEY|ShiftMask,       XK_space,  				togglefloating,				{0} },
	{ MODKEY,                 XK_0,      				view,         				{.ui = ~0 } },
	{ MODKEY|ShiftMask,       XK_0,      				tag,          				{.ui = ~0 } },
	TAGKEYS(                  XK_1,      	                						0)
	TAGKEYS(                  XK_2,      	                						1)
	TAGKEYS(                  XK_3,      	                						2)
	TAGKEYS(                  XK_4,      	                						3)
	TAGKEYS(                  XK_5,      	                						4)
	TAGKEYS(                  XK_6,      	                						5)
	TAGKEYS(                  XK_7,      	                						6)
	TAGKEYS(                  XK_8,      	                						7)
	TAGKEYS(                  XK_9,      	                						8)
	{ MODKEY,                 XK_Left,  				focusmon,     				{.i = -1 } },
	{ MODKEY|ShiftMask,       XK_Left,  				tagmon,       				{.i = -1 } },
	{ MODKEY,                 XK_Right, 				focusmon,     				{.i = +1 } },
	{ MODKEY|ShiftMask,       XK_Right, 				tagmon,       				{.i = +1 } },
	{ MODKEY,			            XK_grave,					focusmon,							{.i = +1 } },
	{ MODKEY|ShiftMask,       XK_grave,					tagmon,								{.i = +1 } 		},

	// Layouts
	{ MODKEY,									XK_t,							setlayout,						{.v = &layouts[0]} },		/* tile */
	{ MODKEY|ShiftMask,				XK_t,							setlayout,						{.v = &layouts[1]} }, 	/* bstack */
	{ MODKEY,									XK_y,							setlayout,						{.v = &layouts[2]} }, 	/* spiral */
	{ MODKEY|ShiftMask,				XK_y,							setlayout,						{.v = &layouts[3]} }, 	/* dwindle */
	{ MODKEY,									XK_u,							setlayout,						{.v = &layouts[4]} }, 	/* deck */
	{ MODKEY|ShiftMask,				XK_u,							setlayout,						{.v = &layouts[5]} }, 	/* monocle */
	{ MODKEY,									XK_i,							setlayout,						{.v = &layouts[6]} }, 	/* centeredmaster */
	{ MODKEY|ShiftMask,				XK_i,							setlayout,						{.v = &layouts[7]} }, 	/* centeredfloatingmaster */
	{ MODKEY,             		XK_f,   	   			togglefullscr,  			{0} },									/* true fullscreen */
	{ MODKEY|ShiftMask,   		XK_f,   	   			setlayout,						{.v = &layouts[8]} },		/* floating */

	// System Functions
	{ MODKEY,									XK_BackSpace,			spawn,								SHCMD("sysact") },

	// Volume
	{ MODKEY,									XK_minus,					spawn,								SHCMD("pamixer --allow-boost -d 2; kill -44 $(pidof dwmblocks);") },
	{ MODKEY|ShiftMask,				XK_minus,					spawn,								SHCMD("pamixer --allow-boost -d 10; kill -44 $(pidof dwmblocks);") },
	{ MODKEY,									XK_equal,					spawn,								SHCMD("pamixer --allow-boost -i 2; kill -44 $(pidof dwmblocks);") },
	{ MODKEY|ShiftMask,				XK_equal,					spawn,								SHCMD("pamixer --allow-boost -i 10; kill -44 $(pidof dwmblocks);") },
	{ MODKEY|ShiftMask,				XK_m,							spawn,								SHCMD("pamixer -t; kill -44 $(pidof dwmblocks);") },
	{ 0, 							XF86XK_AudioLowerVolume,	spawn,								SHCMD("pamixer --allow-boost -d 2; kill -44 $(pidof dwmblocks);") },
	{ 0, 							XF86XK_AudioRaiseVolume,	spawn,								SHCMD("pamixer --allow-boost -i 2; kill -44 $(pidof dwmblocks);") },
	{ 0,							XF86XK_AudioMute,					spawn,								SHCMD("pamixer -t; kill -44 $(pidof dwmblocks);") },
	{ 0,							XF86XK_AudioMicMute,			spawn,								SHCMD("pamixer --default-source -t") },

	// Applications
	{ MODKEY,                 XK_d,							spawn,								{.v = dmenucmd } },
	{ MODKEY,									XK_w,							spawn,								SHCMD("$BROWSER") }, 							// Browser
	{ MODKEY,									XK_r,							spawn,								SHCMD(TERMINAL " -e lfub") }, 		// File Manager
	{ MODKEY|ShiftMask,				XK_r,							spawn,								SHCMD(TERMINAL " -e htop") }, 		// File Manager
	{ MODKEY|ControlMask,			XK_r,							spawn,								SHCMD(TERMINAL " -e pcmanfm") },	// File Manager
	{ MODKEY,									XK_m,							spawn,								SHCMD(TERMINAL " -e ncmpcpp") },

	// Music Control
	{ MODKEY,									XK_p,							spawn,								SHCMD("mpc toggle") },
	{ MODKEY|ShiftMask,				XK_p,							spawn,								SHCMD("mpc pause ; pauseallmpv") },
	{ MODKEY,									XK_bracketleft,		spawn,								SHCMD("mpc seek -10") },
	{ MODKEY|ShiftMask,				XK_bracketleft,		spawn,								SHCMD("mpc seek -60") },
	{ MODKEY,									XK_bracketright,	spawn,								SHCMD("mpc seek +10") },
	{ MODKEY|ShiftMask,				XK_bracketright,	spawn,								SHCMD("mpc seek +60") },
	{ MODKEY,									XK_comma,					spawn,								SHCMD("mpc prev") },
	{ MODKEY|ShiftMask,				XK_comma,					spawn,								SHCMD("mpc seek 0%") },
	{ MODKEY,									XK_period,				spawn,								SHCMD("mpc next") },
	{ MODKEY|ShiftMask,				XK_period,				spawn,								SHCMD("mpc repeat") },

	// Function Keys
	{ MODKEY,									XK_F9,						spawn,								SHCMD("dmenumount") },
	{ MODKEY,									XK_F10,						spawn,								SHCMD("dmenuumount") },
	{ MODKEY,									XK_F11,						spawn,								SHCMD("mpv --no-cache --no-osc --no-input-default-bindings --profile=low-latency --untimed --input-conf=/dev/null --title=webcam av://v4l2:$(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	{ MODKEY,									XK_F12,						spawn,								SHCMD("remaps & notify-send \\\"⌨️ Keyboard remapping...\\\" \\\"Re-running keyboard defaults for any newly plugged-in keyboards.\\\"") },

	{ 0,											XK_Print,					spawn,								SHCMD("maim ~/pic/screenshots/pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,							XK_Print,					spawn,								SHCMD("maim | xclip -selection clipboard -t image/png") },
	{ ControlMask,						XK_Print,					spawn,								SHCMD("maim -s -u | xclip -selection clipboard -t image/png") },
	{ MODKEY,									XK_Print,					spawn,								SHCMD("maimpick") },
	{ 0, 			XF86XK_MonBrightnessUp,						spawn,								SHCMD("light -A 5") },
	{ 0, 			XF86XK_MonBrightnessDown,					spawn,								SHCMD("light -U 5") },
	{ 0, 			XF86XK_Display,										spawn,								SHCMD("displayselect") },

	/* Other Stuff */
	
	// Toggles between Layout and Last Used Layout
	/* { MODKEY,						XK_space,  	setlayout,      {0} }, */

	// Layout Cycling In Array Order
	/* { MODKEY|ControlMask,			XK_comma,  	cyclelayout,    {.i = -1 } }, */
	/* { MODKEY|ControlMask,			XK_period, 	cyclelayout,    {.i = +1 } }, */
	
	// Fine Gap Control
	/* { MODKEY|Mod4Mask,				XK_u,		incrgaps,       {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_u,      	incrgaps,       {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_i,      	incrigaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_i,      	incrigaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_o,      	incrogaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_o,      	incrogaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_6,      	incrihgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_6,      	incrihgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_7,      	incrivgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_7,      	incrivgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_8,      	incrohgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_8,      	incrohgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,				XK_9,      	incrovgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,		XK_9,      	incrovgaps,     {.i = -1 } }, */

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },

	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigstatusbar,   {.i = 6} },
	{ ClkStatusText,        ShiftMask,      Button3,        spawn,          SHCMD(TERMINAL " -e nvim ~/.local/src/dwmblocks/blocks.h") },

	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

